var gulp = require('gulp');
var postcss = require('gulp-postcss');
var sass         = require('gulp-sass');
var autoprefixer = require('autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var plumber = require('gulp-plumber');

gulp.task('sass', function () {
  var plugins = [
      autoprefixer({browsers: ['last 2 version']})
  ];
  return gulp.src('./scss/**/*.scss')
  	.pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(plugins))
    .pipe(plumber.stop())
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./css'));
});
 
gulp.task('default', function () {
  gulp.watch('./scss/**/*.scss', ['sass']);
});